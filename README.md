# Twig WordPress

## Requirements

| Prerequisite    | How to check | How to install
| --------------- | ------------ | ------------- |
| NVM >12.18.4  | `nvm list`    | [NVM](https://github.com/coreybutler/nvm-windows), `nvm install 12.18.4`, this will handle your NODE and NPM packages for you! |
| PHP >= 7.x.x    | `php -v`     | [php.net](http://php.net/manual/en/install.php), if not using a local environment tool|
| MySql >= 5.6.x  |  ---   | --- |

## Features

* [Timber](https://www.upstatement.com/timber/) a faster, easier and more powerful way to build themes, it basically makes sure you can use all the WordPress magic with TWIG 
* [Twig, templating engine](https://twig.symfony.com/),
* [Timber, adds Twig to WordPress!](https://timber.github.io/docs/)
* [Webpack 4](https://webpack.js.org/), bundling assets
* [Bootstrap 4](https://getbootstrap.com/), front-end framework

### Additional Features that are availble if needed!
* [Axios](https://github.com/axios/axios), Promise based HTTP client for the browser and node.js
* [Vue](https://github.com/vuejs/vue),  A progressive, incrementally-adoptable JavaScript framework for building UI on the web.
* [AOS Scroll](https://github.com/michalsnik/aos),  Animate on Scroll
* Linting is using Airbnb-base and Vue-Strongly-recommended, using VsCode, when asked if you want to allow the use of the linting file over your plugin, please select yes ( Allow ). 

## Documentation

### Change your proxy url
* build > webpack.config.js > BrowserSyncPlugin > update to use your currently proxy URL for your local install 

### From the command line
* cd into theme directory
    * npm: `npm ci`
        * will install all your npm packages based on your package-lock and package.json matching up
    * watch: `npm run watch`
        * builds dev assets to public directory and watches assets for changes!
        * sets up browsersync
    * dev: `npm run dev`
        * builds dev assets to public directory
    * prod: `npm run prod`
        * builds production assets to public directory

### Assets Folder
* scripts / css / fonts / images will go inside the /assets directory
    * You'll see multiple folders inside of /assets containing said files
* JS is used along side Dom - Based routing, 
    * You can see this inside of /assets/scripts/pages.js