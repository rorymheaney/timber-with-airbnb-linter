// for ie
// import 'babel-polyfill';
// bootstrap 4 latest iteration
import 'bootstrap';

// pages
import './pages';

// owl
// import 'owl.carousel';

// jquery cookie
// import 'js-cookie';
// window.fancySquareCookies = require('js-cookie');

// images loaded
// enque on pages if needed
// masonry
// enque on pages if needed

// axios
window.axios = require('axios');
