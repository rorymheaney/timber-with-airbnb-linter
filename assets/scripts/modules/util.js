/* eslint-disable no-undef */
// function resizeWidthOnly(start, finish) {
// 	const initialInnerWidth = [window.innerWidth];
// 	return onresize = function () {
// 		const newInnerWidth = window.innerWidth;
// 		const previousInnerWidth = initialInnerWidth.length;
// 		initialInnerWidth.push(newInnerWidth);
// 		if (initialInnerWidth[previousInnerWidth] !== initialInnerWidth[previousInnerWidth - 1]) {
// 			clearTimeout(finish);
// 			finish = setTimeout(start, 1000);
// 		}
// 	}, start;
// }

// bs modal didn't support shift + tab by default
// it would get stuck on the modal, it's a known "issue"
function trapFocus(modal) {
	const focusableEls = modal.find('a[href]:not([disabled]), button:not([disabled]), textarea:not([disabled]), input[type="text"]:not([disabled]), input[type="radio"]:not([disabled]), input[type="checkbox"]:not([disabled]), select:not([disabled]), iframe').filter(':visible');
	const firstFocusableEl = focusableEls[0];
	const lastFocusableEl = focusableEls[focusableEls.length - 1];
	const KEYCODE_TAB = 9;
	// console.log(firstFocusableEl);
	// console.log(lastFocusableEl);
	modal.on('keydown', (e) => {
		const isTabPressed = (e.key === 'Tab' || e.keyCode === KEYCODE_TAB);
		if (!isTabPressed) {
			return;
		}
		// console.log(e.keyCode);
		if (e.shiftKey) /* shift + tab */ {
			if (document.activeElement === firstFocusableEl) {
				lastFocusableEl.focus();
				e.preventDefault();
			}
		} else /* tab */ if (document.activeElement === lastFocusableEl) {
			firstFocusableEl.focus();
			e.preventDefault();
		}
	});
}

// bootstrap carousel ADA updates
// id - carousel id
// currentIndicator - text for buttons, ex: 'current slide is,'
function accessibleCarousel(id, currentIndicator) {
	const $bsCarousel = $(id);
	const $bsSlides = $bsCarousel.find('.carousel-item');
	const $bsIndicators = $bsCarousel.find('[data-js="update-indicator-text"]'); // this data attr was added
	const $bsPausePlay = $bsCarousel.find('[data-js="pause-play-carousel"]'); // pause play
	const $clickElements = $('.bs-carousel__prev, .bs-carousel__next, .bs-carousel__indicator-button');
	let activeSlide;
	//
	// once the carousel has transitioned
	// do the following below

	$bsCarousel.on('slide.bs.carousel', (e) => {
		// current slide, e lists out where the element was, where it's going etc
		// TODO: e.relatedTarget looks like is more efficient as it's
		// the element indexed itself, consider switching
		const currentSlide = e.to;
		activeSlide = e.relatedTarget;

		const lazyLoadedSlide = $(e.relatedTarget).find('[data-bg]');
		if (lazyLoadedSlide.length > 0) {
			// lazyLoadedSlide.attr("src", lazyLoadedSlide.data('src'));
			lazyLoadedSlide.css('background-image', `url(${lazyLoadedSlide.data('bg')})`);
			lazyLoadedSlide.removeAttr('data-bg');
		}
		// reset slides
		$bsSlides.attr('aria-hidden', 'true').attr('tabindex', '-1');
		// update current slide
		$($bsSlides[currentSlide]).attr('aria-hidden', 'false').removeAttr('tabindex');
		// reset indicators
		$bsIndicators.html('');
		// update current indicator
		$($bsIndicators[currentSlide]).html(currentIndicator);
	});

	$bsPausePlay.on('click', function () {
		const $this = $(this);
		// console.log(activeSlide);
		// check if the aria label is play or pause, then play or pause
		if ($this.attr('aria-label') === 'play carousel') {
			$bsCarousel.carousel('cycle');
			$this.attr('aria-label', 'pause carousel');
			$this.find('.fa-pause').removeClass('d-none');
			$this.find('.fa-play').addClass('d-none');
		} else {
			$bsCarousel.carousel('pause');
			$this.attr('aria-label', 'play carousel');
			$this.find('.fa-pause').addClass('d-none');
			$this.find('.fa-play').removeClass('d-none');
		}
	});

	// if it has a tag to hit, then fire
	// delay is .05 longer than transition
	if ($bsCarousel.find('.carousel-item a').length) {
		$clickElements.on('click', () => {
			setTimeout(() => {
				// console.log(activeSlide);
				$(activeSlide).find('a').focus();
			}, 650);
		});
	}
}

function playPauseAosAnimation(AOS, cb) {
	let clicked = false;
	const $animationToggle = $('[data-js="play-pause-aos"]');
	const $aosAttrs = $('[data-aos]');
	const animationClass = 'aos-animate';
	const $pauseText = $('[data-js="toggle-aos-pause-text"]');
	const $playText = $('[data-js="toggle-aos-play-text"]');
	const hideText = 'd-none';

	$animationToggle.on('click', () => {
		if (clicked) {
			$aosAttrs.removeClass(animationClass);
			// will reset all the aos elements and allow you to view them once again
			AOS.refreshHard();
			clicked = false;
		} else {
			// another function passed?
			$aosAttrs.addClass(animationClass);
			clicked = true;
		}
		$pauseText.toggleClass(hideText);
		$playText.toggleClass(hideText);
		// another function passed?
		// example, see home page,
		// passes click function to pause carousel when all animations paused etc
		if (cb) {
			cb();
		}
	});
}

function closeChatButton() {
	const $closeButton = $('[data-js="close-chat-button"]');
	const $saleForceButton = $('[data-js="sales-force-chat"]');
	$closeButton.on('click', (event) => {
		event.stopPropagation();
		$saleForceButton.removeClass('sales-force-chat--showing');

		$saleForceButton.addClass('d-none');
		// eslint-disable-next-line no-undef
		cieCookies.set('closed_chat', 'cookie_was_closed');
	});
	// eslint-disable-next-line no-undef
	if (cieCookies.get('closed_chat') === undefined) {
		$saleForceButton.removeClass('d-none');
	}
}

export {
	// resizeWidthOnly,
	trapFocus,
	accessibleCarousel,
	playPauseAosAnimation,
	closeChatButton
};
