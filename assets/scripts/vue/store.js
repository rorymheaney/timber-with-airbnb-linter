import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		doctor: '',
		doctors: '',
		showDoctors: false,
		showDoctor: false
	},
	mutations: {
		DOCTORS_VISIBILITY(state, payload) {
			state.showDoctors = payload;
		},
		SET_DOCTORS(state, payload) {
			state.doctors = payload;
		},
		DOCTOR_VISIBILITY(state, payload) {
			state.showDoctor = payload;
		},
		SET_DOCTOR(state, payload) {
			state.doctor = payload;
		}
	},
	actions: {}
});
