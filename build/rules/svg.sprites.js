const config = require('../app.config');

module.exports = {
	test: /\.svg$/,
	include: config.paths.images,
	exclude: config.paths.webfonts,
	loader: 'svg-sprite-loader',
	options: {}
};
