<?php

// add ACF for global calls
// {{options.your_field_bro}}

add_filter( 'timber_context', 'fancySquares_acf_options_timber_context'  );

function fancySquares_acf_options_timber_context( $context ) {
    $context['options'] = get_fields('option');
    return $context;
}
//
// add google maps key
// function my_acf_google_map_api( $api ){
//     $api['key'] = 'AIzaSyDwjhCdTUQnV8ccqflRfl9lrjIcvalWqM8';
//     return $api;
// }
// add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

// add acf meta filters to queries https://www.advancedcustomfields.com/resources/query-posts-custom-fields/
//
// filter by affiliations

