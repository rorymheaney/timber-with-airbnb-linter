<?php
//
//
// make jpeg quality 100 be default, smush after
//
//
add_filter('jpeg_quality', function($arg){return 100;});


//
//
// enque scripts
//
//
add_action( 'wp_enqueue_scripts', 'fancySquares_enqueue_scripts' );

function fancySquares_enqueue_scripts() {
	// wp_enqueue_script('polyfill', 'https://polyfill.io/v3/polyfill.min.js?features=Object.assign%2CPromise%2CPromise.prototype.finally', '', '', false);
	
	// wp_enqueue_script( 'underscore' );
	wp_enqueue_script("vue-js", get_template_directory_uri () . "/public/scripts/vue.js",'',filemtime(get_stylesheet_directory() . '/public/scripts/vue.js'), true);
    // if(is_single()){
    //      wp_enqueue_script( 'wp-mediaelement' );
    //      wp_enqueue_style( 'wp-mediaelement' );
    // }


}
function replace_core_jquery_migrate(){
	wp_deregister_script( 'jquery-migrate' );
	wp_enqueue_script( 'jquery-migrate', get_template_directory_uri () . "/public/vendor/jquery-migrate-3.2.0.min.js", array(), '3.2.0', false );
}
add_action( 'wp_enqueue_scripts', 'replace_core_jquery_migrate', 10 );

function replace_core_jquery_version() {
	wp_deregister_script( 'jquery-core' );
	wp_register_script( 'jquery-core', get_template_directory_uri () . "/public/vendor/jquery-3.4.1.min.js", array(), '3.4.1', false );
	array_unshift(wp_scripts()->queue, 'jquery-core');
}
add_action( 'wp_enqueue_scripts', 'replace_core_jquery_version', 0 );


//gravity forms footer
add_filter( 'gform_init_scripts_footer', '__return_true' );